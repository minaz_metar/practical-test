import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(
    public http: HttpClient,

  ) { }

  getCategoryDetails() {
    return this.http.post('http://192.241.153.53:3017/api/getMenuCategory', JSON.stringify({})).pipe(
      map((res: Response) => {
        return res;
      }),
      catchError(err => { return err; }));
  }

  getHomeDetails() {
    return this.http.post('http://192.241.153.53:3017/api/getHomeData', JSON.stringify({})).pipe(
      map((res: Response) => {
        return res;
      }),
      catchError(err => { return err; }));
  }
}
