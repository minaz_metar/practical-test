import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel-basic',
  templateUrl: './carousel-basic.component.html',
  styleUrls: ['./carousel-basic.component.scss']
})
export class CarouselBasicComponent implements OnInit {
  @Input() inputSliderData: any;
  sliderData: any;
  constructor() { }

  ngOnChanges(changes) {
    if (this.inputSliderData) {
      this.sliderData = this.inputSliderData[0].data
      this.sliderData.forEach(element => {
        element["img"] = "../../assets/images/1.jpg"
        if (element.id === 2 || element.id === 1) {
          element["img"] = "../../assets/images/3.jpg"
        }
      });
    }
  }

  ngOnInit(): void {
  }

}
