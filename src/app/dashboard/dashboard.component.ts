import { ApiCallService } from './../shared/services/api-call.service';
import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public menuList: any = [];
  public homeDetails: any = []
  public sliderDetails: any;
  public bannerData: any;
  public offerDetails: any;
  public currentItem = {
    'name': 'Electronics',
    data: []
  }
  constructor(
    public apiCallService: ApiCallService
  ) { }

  ngOnInit(): void {
    this.getHomeDetails();
    this.getCategoryDetails()

  }

  getCategoryDetails() {
    this.apiCallService.getCategoryDetails().subscribe((res: any) => {
      if (res.status && res.data) {
        this.menuList = res.data;
      }
      // console.log(res);

    })
  }

  getHomeDetails() {
    this.apiCallService.getHomeDetails().subscribe((res: any) => {
      if (res.status && res.homeData) {
        this.homeDetails = res.homeData;
        this.sliderDetails = this.homeDetails.filter(id => id.viewType === 'slider')
        this.bannerData = this.homeDetails.filter(id => id.viewType === 'banner')[0].data;
        this.offerDetails = this.homeDetails.filter(id => id.viewType === 'offers')[0].data;
        this.currentItem.data = _.where(this.homeDetails, { 'name': 'electronics' })[0].data;
        this.currentItem.data.forEach(e => {
          e["img"] = "../../assets/images/l1.jpg"
          if (e.category_name.indexOf('Desktop') > -1) {
            e["img"] = "../../assets/images/d.jpg"

          }
        })
      }
      this.bannerData.forEach(element => {
        if (element.sort_description.indexOf("Baby & Kids") > -1) {
          element["bannerImg"] = "../../assets/images/5.jpg"
        } else if (element.sort_description.indexOf("Electronics") > -1) {
          element["bannerImg"] = "../../assets/images/2.jpg"
        } else if (element.sort_description.indexOf("Home") > -1) {
          element["bannerImg"] = "../../assets/images/f2.jpg"
        } else {
          element["bannerImg"] = "../../assets/images/m2.jpg"
        }
      });
      console.log(this.bannerData);

    })
  }

  fetchDetails(item) {
    this.currentItem.data = [];
    this.currentItem.name = item.name;
    this.currentItem.data = _.where(this.homeDetails, { '_id': item._id })[0].data;

  }

}
